#!/bin/bash

cd "$(dirname "$0")"

# turn on verbose debugging output for parabuild logs.
set -x
# make errors fatal
set -e

HUNSPELL_VERSION="1.3.2"
HUNSPELL_SOURCE_DIR="hunspell-1.3.2"

if [ -z "$AUTOBUILD" ] ; then
    fail
fi

if [ "$OSTYPE" = "cygwin" ] ; then
    export AUTOBUILD="$(cygpath -u $AUTOBUILD)"
fi

# load autbuild provided shell functions and variables
set +x
eval "$("$AUTOBUILD" source_environment)"
set -x
build_linux()
{
    # Prefer gcc-4.6 if available.
    if [[ -x /usr/bin/gcc-4.6 && -x /usr/bin/g++-4.6 ]]; then
      export CC=/usr/bin/gcc-4.6
      export CXX=/usr/bin/g++-4.6
    fi
    libdir="$stage/lib"
    export CFLAGS="-m$1 $2" CXXFLAGS="-m$1 $2"
    export LFLAGS="-m$1"
    ./configure --prefix="$stage"
    make
    make install
    mv "$stage/lib" "$stage/release"
    mkdir -p "$stage/lib"
    mv "$stage/release" "$stage/lib"
}

stage="$(pwd)/stage"

build=${AUTOBUILD_BUILD_ID:=0}
echo "${HUNSPELL_VERSION}.${build}" > "${stage}/VERSION.txt"

pushd "$HUNSPELL_SOURCE_DIR"
    case "$AUTOBUILD_PLATFORM" in
        "windows")
            load_vsvars

            build_sln "src/win_api/hunspell.sln" "Debug_dll|Win32"
            build_sln "src/win_api/hunspell.sln" "Release_dll|Win32"

            mkdir -p "$stage/lib/debug"
            mkdir -p "$stage/lib/release"
            cp src/win_api/Debug_dll/libhunspell/libhunspell{.dll,.lib,.pdb} "$stage/lib/debug"
            cp src/win_api/Release_dll/libhunspell/libhunspell{.dll,.lib,.pdb} "$stage/lib/release"
        ;;
        "darwin")
            opts='-arch i386 -iwithsysroot /Developer/SDKs/MacOSX10.9.sdk -mmacosx-version-min=10.7'
            export CFLAGS="$opts"
            export CXXFLAGS="$opts"
            export LDFLAGS="$opts"
            ./configure --prefix="$stage"
            make
            make install
            mkdir -p "$stage/lib/release"
            mv "$stage/lib/"{*.a,*.dylib,*.alias} "$stage/lib/release"
            pushd "$stage/lib/release"
              fix_dylib_id libhunspell-1.3.0.dylib
            popd
        ;;
        "linux")
            build_linux 32
        ;;
        "linux64")
            build_linux 64 -fPIC
        ;;
    esac
    mkdir -p "$stage/include/hunspell"
    cp src/hunspell/{*.h,*.hxx} "$stage/include/hunspell"
    cp src/win_api/hunspelldll.h "$stage/include/hunspell"
    mkdir -p "$stage/LICENSES"
    cp "license.hunspell" "$stage/LICENSES/hunspell.txt"
    cp "license.myspell" "$stage/LICENSES/myspell.txt"
    README_DIR="$stage/autobuild-bits"
    README_FILE="$README_DIR/README-Version-hunspell"
    mkdir -p $README_DIR
    cat ../.hg/hgrc|grep default |sed  -e "s/default = ssh:\/\/hg@/https:\/\//" > $README_FILE
    echo "Commit $(hg id -i)"|sed  -e "s/\+//" >> $README_FILE
popd

pass
